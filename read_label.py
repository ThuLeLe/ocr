import os
import json
import numpy as np
import cv2
import shutil

def yolo_point(coordinate, h, w):
    print(np.array(coordinate)[:,0])
    minx = min (np.array(coordinate)[:,0])
    print(coordinate)
    print(minx)
    maxx = max (np.array(coordinate)[:,0])
    miny = min(np.array(coordinate)[:,1])
    maxy = max(np.array(coordinate)[:,1])
    xcenter = ((maxx +  minx) / 2)/w
    ycenter = ((maxy +  miny) / 2)/h
    width = (maxx-minx)/w
    height = (maxy-miny)/h
    print(ycenter)

    return xcenter, ycenter, width, height


count = 0
path_label = "/home/thule/Project/ocr/yolo/CMND/single/labels" #Label path for train *****Change path 
path_img = "/home/thule/Project/ocr/yolo/CMND/single/images" # Image path for train *****Change path 


# Open the label file of Paddle 
with open ("/home/thule/Project/ocr/yolo/CMND/vinh/single/vc_card_split_100_021/Label.txt") as file: #*****Change path 
    for line in file:
       content = line.split("\t")
       name_image = content[0] 
       name_image=(name_image.split("/")[-1])
       # Copy image from Paddle 
       shutil.copy2(os.path.join("/home/thule/Project/ocr/yolo/CMND/vinh/single/vc_card_split_100_021", name_image), path_img) #*****Change path 
       name_file = name_image.replace("jpg", "") + "txt"
       print(name_file)
       
       # Create text file with name of name_image
       f = open(os.path.join(path_label, name_file), "w+")
       # Take info of image
       info = content[1]
       data = json.loads(info)
       numb_box = len(data)
       coordinate = data[0]['points']

       # Find image dimension        
       im = cv2.imread(os.path.join(path_img, name_image))
       h, w, c = im.shape

       # Convert paddle coordinate to yolo and write to text 
       for i in range(numb_box):
           print(name_file)
           coordinate = data[i]['points']
           xcenter, ycenter, width, height = yolo_point(coordinate, h, w)
           f.write("0 ")
           f.write(str(xcenter))
           f.write(" ")
           f.write(str(ycenter))
           f.write(" ")
           f.write(str(width))
           f.write(" ")
           f.write(str(height))
           f.write(" ")
           f.write("\n")


